﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3ShapeAutomation.Pages
{
    public class RequestDemoPage
    {
        private readonly IWebDriver _driver;
        public RequestDemoPage(IWebDriver driver) => _driver = driver;

        By requestDemoBtnBy = By.XPath("//*[contains(@class,'headerNavigation_callToAction')]");
        By firstNameBy = By.XPath("//*[@data-systemname='sha_firstname']");
        By lastNameBy = By.XPath("//*[@data-systemname='sha_lastname']");

        public void RequestDemo()
        {
            _driver.FindElement(requestDemoBtnBy).Click();
        }

        public void EnterFirstName(string firstName)
        {
            Waiters.FindElement(_driver,firstNameBy).SendKeys(firstName);
        }

        public void EnterLastName(string lastName)
        {
            Waiters.FindElement(_driver, lastNameBy).SendKeys(lastName);
        }
    }
}
