﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _3ShapeAutomation.Pages
{
    public class LandingPage
    {

        private readonly IWebDriver _driver;
        public LandingPage(IWebDriver driver) => _driver = driver;

        By logoBy = By.XPath("//*[contains(@class,'headerLogo')]");
        By acceptAllBtnBy = By.XPath("(//*[contains(@class,'accept')])[1]");
        By searchBtnBy = By.XPath("//*[contains(@class,'openSearch')]");
        By searchFieldBy = By.XPath("(//input[@placeholder='Type to search'])[1]");
        By searchResultsBy = By.XPath("//*[contains(@class,'searchResults_title')]");
        By watchVideoBtnBy = By.XPath("//h2[contains(text(),'Digital dentistry training')]/following-sibling::button");

        public void GoTo(string url)
        {
            _driver.Manage().Window.Maximize();
            _driver.Navigate().GoToUrl(url);
            Waiters.WaitForPageLoad(_driver);
        }

        public Boolean isLogoDisplayed()
        {
            return _driver.FindElement(logoBy).Displayed;
        }

        public void AcceptAllCookies()
        {
            _driver.FindElement(acceptAllBtnBy).Click();
        }

        public void SearchByText(string text)
        {
            _driver.FindElement(searchBtnBy).Click();
            var searchField = _driver.FindElement(searchFieldBy);
            searchField.Clear();
            searchField.SendKeys(text);
            searchField.SendKeys(Keys.Enter);
            Waiters.WaitForBlockerAppeared(_driver, 3);
            Waiters.WaitForUIBlockerDissapeared(_driver);
        }

        public List<string> GetSearchResultsList()
        {
            var searchResults = _driver.FindElements(searchResultsBy);
            List<string> searchResultsList = new List<string>();
            foreach (var item in searchResults)
            {
                searchResultsList.Add(item.GetAttribute("innerText"));
            }
            return searchResultsList;
        }

        public void ClickOnWatchVideoButton()
        {
            Waiters.FindElement(_driver, watchVideoBtnBy).Click();
            Waiters.WaitForPageLoad(_driver);
        }

        public void CheckVideoPlaying()
        {
            //_driver.Navigate().GoToUrl("https://www.w3.org/2010/05/video/mediaevents.html");
            IJavaScriptExecutor jse = (IJavaScriptExecutor) _driver;
            //Click on play button
            jse.ExecuteScript("document.getElementById(\"wistia_simple_video_49\").play()");
            Thread.Sleep(2000);
            jse.ExecuteScript("document.getElementById(\"wistia_simple_video_49\").pause()");
            Thread.Sleep(2000);
        }

        public void ClickOnFirstItemInSearchResults()
        {
            Waiters.FindElements(_driver, searchResultsBy)[0].Click();
            Waiters.WaitForPageLoad(_driver);
        }
    }
}
