﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;

namespace _3ShapeAutomation
{
    public class Waiters
    {
        private readonly IWebDriver _driver;
        public Waiters(IWebDriver driver) => _driver = driver;

        public static IWebElement FindElement(IWebDriver driver, By by, int secondsToWait = 40)
        {
            if (secondsToWait > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(secondsToWait));

                return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(by));
            }
            return driver.FindElement(by);
        }

        public static IList<IWebElement> FindElements(IWebDriver driver, By by, int secondsToWait = 40)
        {
            if (secondsToWait > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(secondsToWait));
                return wait.Until(drv => drv.FindElements(by));
            }
            return driver.FindElements(by);
        }

        public static void WaitForPageLoad(IWebDriver driver)
        {
            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30.00));
            wait.Until(driver1 => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public static void WaitTextIsPresentInSearchResults(IWebDriver driver, By locator, string text, int secondsToWait = 40)
        {
            Waiters.FindElement(driver, locator, 30);
            new WebDriverWait(driver, new TimeSpan(0, 0, secondsToWait))
               .Until(d => d.FindElement(locator).GetAttribute("innerHTML").Contains(text));
        }

        public static void WaitForListItemsMoreThan0(IWebDriver driver, By locator, int secondsToWait = 40)
        {
            new WebDriverWait(driver, new TimeSpan(0, 0, secondsToWait))
               .Until(d => d.FindElements(locator).Count > 0);
        }

        public static void WaitForListItemsExpectedCount(IWebDriver driver, By locator, int expectedCount, int secondsToWait = 40)
        {
            new WebDriverWait(driver, new TimeSpan(0, 0, secondsToWait))
               .Until(d => d.FindElements(locator).Count == expectedCount);
        }

        public static void WaitForTextToBePresent(IWebDriver driver, By locator, string text, int secondsToWait = 40)
        {
            new WebDriverWait(driver, new TimeSpan(0, 0, secondsToWait))
               .Until(SeleniumExtras.WaitHelpers.ExpectedConditions.TextToBePresentInElementLocated(locator, text));
        }

        public static bool WaitForSpinnerDissapeared(IWebDriver driver, int secondsToWait = 40)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(secondsToWait));
                wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                By by = By.XPath("//*[@class='Spinner ']");
                var present = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(by));
                return present;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
            }
        }

        public static bool ElementDisplayed(IWebDriver driver, By by, int secondsToWait = 2)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(secondsToWait));

            try
            {
                return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by)).Displayed;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public static bool WaitForElementIsNotDisplayed(IWebDriver driver, By by, int secondsToWait = 2)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(secondsToWait));
            try
            {
                return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(by));
            }
            catch (Exception e)
            {
                Console.Write(e);
                return false;
            }
        }

        public static bool WaitForBlockerDissapeared(IWebDriver driver, By by, int secondsToWait = 40)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(secondsToWait));
                wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                var present = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(by));
                return present;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
            }
        }

        public static bool WaitForUIBlockerDissapeared(IWebDriver driver, int secondsToWait = 40)
        {
            By by = By.XPath("//*[contains(@class,'c-loader')]|//*[@class='Spinner-inline']");
            return WaitForBlockerDissapeared(driver, by);
        }

        public static bool WaitForBlockerAppeared(IWebDriver driver, int secondsToWait = 40)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(secondsToWait));
                wait.IgnoreExceptionTypes(typeof(StaleElementReferenceException));
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                By by = By.XPath("//*[contains(@class,'c-loader')]");
                var present = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by)).Displayed;
                return present;
            }
            catch (Exception e)
            {
                Console.Write("test for blocker waiter");
                return false;
            }
            finally
            {
            }
        }
    }
}
