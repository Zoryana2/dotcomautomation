﻿using _3ShapeAutomation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Tests.Steps
{
    [Binding]
    public class RequestDemoSteps : BaseSteps
    {
        RequestDemoPage requestDemoPage;
        public RequestDemoSteps(SeleniumContext seleniumContext, TestContext testContext) : base(seleniumContext, testContext)
        {
            requestDemoPage = new RequestDemoPage(_driver);
        }

        [When(@"I clicked Request Demo button")]
        public void WhenIClickedRequestDemoButton()
        {
            requestDemoPage.RequestDemo();
        }

        [Given(@"I entered (.*) first name and (.*) last name")]
        public void GivenIEnteredFirstNameAndLastNameLastName(string firstName, string lastName)
        {
            requestDemoPage.EnterFirstName(firstName);
            requestDemoPage.EnterLastName(lastName);
        }


    }
}
