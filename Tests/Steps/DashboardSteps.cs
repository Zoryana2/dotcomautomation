﻿using _3ShapeAutomation.Pages;
using System.Configuration;
using TechTalk.SpecFlow;
using FluentAssertions;

namespace Tests.Steps
{
    [Binding]
    class DashboardSteps : BaseSteps
    {
        LandingPage landingPage;

        public DashboardSteps(SeleniumContext seleniumContext, TestContext testContext) : base(seleniumContext, testContext)
        {
            landingPage = new LandingPage(_driver);
        }

        [Given(@"I have opened landing page")]
        public void GivenIHaveOpenedLandingPage()
        {
            var url = ConfigurationManager.AppSettings["Url"]; ;
            landingPage.GoTo(url);
        }

        [Then(@"browser title is correct")]
        public void ThenBrowserTitleIsCorrect()
        {
            var title = _driver.Title;
            title.Should().Be("3Shape - 3D Dental Scanners & CAD/CAM Systems");
        }

        [Then(@"(.*)Shape logo is displayed")]
        public void ThenShapeLogoIsDisplayed(int p0)
        {
            landingPage.isLogoDisplayed().Should().BeTrue("Logo should be displayed on landing page");
        }

        [Given(@"I accepted all cookies")]
        public void GivenIAcceptedAllCookies()
        {
            landingPage.AcceptAllCookies();
        }

        [Given(@"I have searched by parameter (.*)")]
        [When(@"I have searched by parameter (.*)")]
        public void WhenIHaveSearchedByParameter(string parameter)
        {
            landingPage.SearchByText(parameter);
        }

        [Then(@"search results should contain (.*)")]
        public void ThenSearchResultsShouldContainAutoTester(string parameter)
        {
            var searchResultsList = landingPage.GetSearchResultsList();
            foreach (var item in searchResultsList)
            {
                item.ToLower().Contains(parameter.ToLower()).Should().BeTrue("Search results should contain: " + parameter);
            }         
        }

        [When(@"I clicked Watch video button")]
        public void WhenIClickedWatchVideoButton()
        {
            landingPage.ClickOnWatchVideoButton();
        }

        [Then(@"Video should be playing succesfully")]
        public void ThenVideoShouldBePlayingSuccesfully()
        {
            landingPage.CheckVideoPlaying();
           
        }

        [When(@"I have selected first search result")]
        public void WhenIHaveSelectedFirstSearchResult()
        {
            landingPage.ClickOnFirstItemInSearchResults();
        }

    }
}
