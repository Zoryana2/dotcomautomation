﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Steps
{
    public class SeleniumContext
    {
        public IWebDriver _driver { get; private set; }

        public SeleniumContext()
        {
            var browserType = ConfigurationManager.AppSettings["browser"];
            var downloadDirectory = ConfigurationManager.AppSettings["downloadFolder"];
            SelectBrowser(browserType, downloadDirectory);
        }

        internal void SelectBrowser(string browserType, string downloadDirectory)
        {
            var browserMode = ConfigurationManager.AppSettings["browserMode"];

            switch (browserType)
            {
                case "Chrome":
                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", downloadDirectory);
                    chromeOptions.AddUserProfilePreference("download.prompt_for_download", false);
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");
                    if (browserMode == "headless")
                        chromeOptions.AddArgument(browserMode);
                    _driver = new ChromeDriver(chromeOptions);
                    break;
                case "IE":
                    InternetExplorerOptions IEoptions = new InternetExplorerOptions();
                    IEoptions.AddAdditionalCapability("download.default_directory", downloadDirectory);
                    _driver = new InternetExplorerDriver(IEoptions);
                    break;
                case "Firefox":
                    FirefoxOptions profile = new FirefoxOptions();
                    profile.SetPreference("browser.download.dir", downloadDirectory);
                    _driver = new FirefoxDriver(profile);
                    break;
            }
            _driver.Manage().Window.Size = new System.Drawing.Size(1920, 1080);
        }
    }
}
