﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Steps
{
    public class BaseSteps
    {
        protected TestContext _testContext;
        protected SeleniumContext seleniumContext;
        protected IWebDriver _driver;

        public BaseSteps(SeleniumContext seleniumContext, TestContext testContext)
        {
            this.seleniumContext = seleniumContext;
            _driver = seleniumContext._driver;
            _testContext = testContext;
        }
    }
}
