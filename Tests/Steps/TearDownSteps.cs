﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Tests.Steps
{

    [Binding]
    class TearDownSteps : BaseSteps
    {
        public TearDownSteps(SeleniumContext seleniumContext, TestContext testContext) : base(seleniumContext, testContext)
        {
        }

        [AfterScenario]
        public void RunAfterScenario()
        {
            seleniumContext._driver.Close();
            seleniumContext._driver.Quit();
        }
    }
}
