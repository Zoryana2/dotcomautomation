﻿Feature: Dashboard

@mytag
Scenario: Open landing page 
	Given I have opened landing page
	Then browser title is correct
	And 3Shape logo is displayed 


@Search @Acceptance
Scenario Outline: Search with different search parameters
	Given I have opened landing page
	And I accepted all cookies
	When I have searched by parameter <SearchParameter>
	Then search results should contain <SearchParameter>
Examples:
| SearchParameter   |
| Dental            |
| Trios             |
| Ortho             |
| connection        |

Scenario: Test video Digital dentistry training
	Given I have opened landing page
	And I accepted all cookies
	When I have searched by parameter Digital dentistry training for dental professionals
	And I have selected first search result
	Given I accepted all cookies
	When I clicked Watch video button
	Then Video should be playing succesfully